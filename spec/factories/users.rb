# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  sequence(:email) {|n| "person#{n}@example.com" }
  sequence(:name) {|n| "Person #{n}" }
  
  factory :visit do
    total 21234567
    premium 98374
    revenue 320
    created_at Date.today.prev_day
    user
  end

  factory :profile do
    rate 3.5
    nprate 1.0
    level 'Gold'
    campaign '1'
    user
  end

  factory :user do
    name
    email
    password 'changeme'
    password_confirmation 'changeme'
    confirmed_at Time.now

    ignore do
      visits_count 5
    end


    after(:create) do |user, evaluator|
      FactoryGirl.create(:profile, user: user)
      FactoryGirl.create_list(:visit, evaluator.visits_count, user: user)
    end

  end
end