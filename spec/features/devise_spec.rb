require 'spec_helper'

feature "Users' Sign Up and Sign In" do
  	
  	let(:user) { FactoryGirl.create(:user) }
    scenario "Signing in with existing user" do
      as_user(user).visit root_path
      expect(page).to have_content('Sign out')
    end

    scenario "Visiting root page without exisitng user" do
    	as_visitor(user).visit root_path
    	expect(page).to have_content('Sign in')
    end

end
