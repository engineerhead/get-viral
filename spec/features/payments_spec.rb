require "spec_helper"

feature "Payments history" do 
	
	background(:all) do 
		# Get Gold Users
		FactoryGirl.create_list(:user, 3)

		cd = Date.today
		bw = cd.beginning_of_week 
		ew = cd.end_of_week
		period = bw.strftime("%d/%m/%Y") + '-' + ew.strftime("%d/%m/%Y")

		users_revenue = Hash.new(0)
		all_visits = Visit.select('user_id,revenue').in_range(bw,ew)
		all_visits.each do |e|  
			users_revenue[e.user_id] += e.revenue
		end

		ActiveRecord::Base.transaction do
			users_revenue.each do |id,revenue|
				Payment.create(
					user_id: id,
					amount: revenue,
					period: period,
					paid: false
				)
			end
		end

	end
	
	let!(:admin) { FactoryGirl.create(:user, admin: true) }

	scenario "Listing paymets" do
		as_user(admin).visit payments_path
		expect(page).to have_selector('tr', count: 4)
	end

	scenario "Listing paymets for specfied user" do
		as_user(admin).visit payments_path
		select('Person 1', from: '_payments_user')
		click_button('Update')
		expect(page).to have_selector('tr', count: 2)
	end

	scenario "Editing paymets" do
		as_user(admin).visit payments_path
		click_link('Edit')
		expect(page).to have_content('tr')
	end

end