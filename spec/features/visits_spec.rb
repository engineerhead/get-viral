require 'spec_helper'

include AnalyticsHelpers

feature "User's Access to Visits Dashboard" do
  	
  background(:all) do
    premium_visits = [["1","sm","76616"],["10-hottest-disney-princess-look-alike-models","rss","23"],["2.8","2.8","14"],["33-6","33","9"],["666","666","23"],["800","800","28"],["99","99","110"],["99-9","99","41"],["AK","HM","4595"],["AKK","MU-L","9746"],["AKS","MU-L","69"],["AS","MU-L","25058"],["AS2","MU-L","55165"],["AS3","MU-L","9"],["AVI","MU-L","8809"],["AW","AW","14"],["AW","MU-L","29993"],["Abdullah","MU-L","1590"],["Adnan","MU-L","23670"],["Amjad","MU-L","25839"],["Areef","MU-L","184"],["Arslan","MU-L","5657"],["BAB","MU-L","19663"],["Babar","MU-L","8331"],["Bilal","MU-L","538"],["Charming","MU-L","2946"],["Code","MU-L","10220"],["Danish","MU-L","10762"],["ES","MU-L","32"],["FMD","HM","18712"],["Faisal","MU-L","694"],["Farooq","MU-L","20963"],["HM","HM","8276"],["HZ","MU-L","8970"],["Haroon","MU-L","15872"],["Haseeb","PW-L","7522"],["Hassan","MU-L","2734"],["Inam","MU-L","8331"],["Irfan","MU-L","6681"],["Irfan2","MU-L","7568"],["JAM","MU-L","55"],["Jamil","MU-L","41"],["Jazi","MU-L","32"],["Jazi","MU-S","6939"],["Malik","MU-L","685"],["Middle","MU-L","23514"],["Middle","mu-l","9"],["MuDan","MuDan","18"],["Mubeen","PW-L","5284"],["Mushy","MU-L","15137"],["Nomi","MU-L","6966"],["Noor","MU-L","2334"],["PW","PW-L","10684"],["Prince","MU-L","3437"],["RM","RM","3933"],["Rani","MU-L","6116"],["Rashid","MU-L","9"],["SD","SD","87"],["SDA","MU-L","8689"],["SDA2","MU-L","7384"],["SID","MU-L","23"],["Salman1","MU-L","72291"],["Salman10","MU-L","8129"],["Salman11","MU-L","1778"],["Salman12","MU-L","2137"],["Salman13","MU-L","6006"],["Salman14","MU-L","3529"],["Salman2","MU-L","61635"],["Salman3","MU-L","11516"],["Salman4","MU-L","6750"],["Salman5","MU-L","20173"],["Salman6","MU-L","39307"],["Salman7","MU-L","5298"],["Salman8","MU-L","6171"],["Salman9","MU-L","2132"],["Sami","MU-L","9"],["Sarim","MU-L","14953"],["Sheyan","MU-L","13161"],["Sony","PW-L","12338"],["Sony","PW-S","32"],["Spice","MU-L","69998"],["TBBT","HM","18486"],["Tariq","MU-L","60"],["Ttp","MU-L","165"],["U-K","HM","7472"],["UK","MU-L","18"],["VVK","HM","51"],["Wajahat","MU-L","2454"],["ZC","ZC","79938"],["ZF","ZC","29781"],["ZH","MU-L","8667"],["Zaheer","MU-L","21326"],["Zahid","MU-L","3377"],["Zain","MU-L","8961"],["Zia","MU-L","5629"],["ab","ab","51"],["celebrities-photoshop","rss","3001"],["celebs-hanging-out","rss","32"],["celebs-stuck-in-time","rss","1958"],["crazy-emo-photos","rss","18"],["creepy-russians","rss","18"],["dc","dc","207"],["ek","ek","64"],["epic-dating-fails","rss","74"],["food-tattoos","rss","55"],["gree","PW-L","14323"],["gym-fails","rss","3911"],["gym-fails?epic=mu","rss","14"],["hs","hs","30241"],["iconic-photos-colorized","rss","2707"],["lt","lt","34749"],["mu","MU-L","23"],["nice-try-but","rss","9"],["rehan","MU-L","11649"],["relationship-totally-love","rss","64"],["sf","sf","78"],["shiz","PW-L","19415"],["slownsteady","MU-L","23647"],["stupid-crazy-both","rss","18"],["when-drunk-wasted","rss","3290"],["when-drunk-wasted?epic=mu","rss","9"],["worst-parenting-fails-2","rss","28"],["wtf-photos-2","rss","3956"],["wtf-pics","rss","74"],["wtf-pregnancy-pics","rss","41"]]
    total_visits = [["1","114543"],["10-hottest-disney-princess-look-alike-models","6"],["2.8","18"],["33","8"],["33-6","17"],["33Ads","15"],["666","33"],["800","21"],["99","128"],["99-9","44"],["AK","5603"],["AKK","11686"],["AKS","100"],["AS","41158"],["AS1","10"],["AS2","97694"],["AS3","14"],["AVI","9693"],["AW","39486"],["Ab","28"],["Abdullah","3721"],["Adnan","26779"],["Amjad","29118"],["Areef","425"],["Arslan","6057"],["BAB","20719"],["Babar","14111"],["Bilal","848"],["Charming","3113"],["Code","12438"],["Danish","17123"],["ES","57"],["FMD","21447"],["Faisal","1277"],["Farooq","25010"],["HM","9293"],["HZ","12474"],["Haroon","20617"],["Haseeb","8892"],["Hassan","6902"],["Inam","8856"],["Irfan","12322"],["Irfan2","41766"],["JAM","53"],["Jamil","58"],["Jazi","8628"],["Junaid","10"],["Malik","815"],["Middle","25040"],["MuDan","14"],["Mubeen","8144"],["Mushy","22524"],["Nomi","9495"],["Noor","2989"],["PW","12679"],["Prince","5505"],["RM","7651"],["Rani","16227"],["Rashid","15"],["SD","122"],["SDA","9401"],["SDA2","7882"],["SID","43"],["Salman1","89517"],["Salman10","10632"],["Salman11","2878"],["Salman12","6058"],["Salman13","7485"],["Salman14","5339"],["Salman2","74694"],["Salman3","13348"],["Salman4","8526"],["Salman5","20760"],["Salman6","46691"],["Salman7","6342"],["Salman8","9108"],["Salman9","3094"],["Sami","8"],["Sarim","21381"],["Sheyan","15811"],["Shiz","8"],["Sony","16098"],["Spice","119821"],["TBBT","25911"],["Tariq","104"],["Ttp","324"],["U-K","7869"],["UK","18"],["VVK","106"],["Wajahat","3781"],["Z","13"],["ZC","148879"],["ZF","38337"],["ZH","17737"],["Zaheer","27027"],["Zahid","4248"],["Zain","11865"],["Zia","7294"],["ab","76"],["art-trolling","40"],["celebrities-photoshop","4789"],["celebrities-photoshop?epic=mu","21"],["celebrities-photoshop?epic=sf","6"],["celebs-hanging-out","78"],["celebs-stuck-in-time","3534"],["celebs-stuck-in-time?epic=mu","7"],["celebs-thin-fat","53"],["crazy-emo-photos","31"],["creepy-russians","26"],["dc","242"],["drunk-pranked","8"],["ek","95"],["epic-dating-fails","180"],["food-tattoos","92"],["gree","25716"],["gym-fails","6589"],["gym-fails?epic=mu","59"],["gym-fails?epic=sf","7"],["gym-fails?epic=sm","6"],["hs","58157"],["iconic-photos-colorized","3856"],["iconic-photos-colorized?epic=mu","13"],["lt","59970"],["mu","62"],["people-should-be-allowed-on-internet","10"],["rehan","14546"],["relationship-totally-love","134"],["sf","94"],["shiz","22324"],["shlt-happens","9"],["slownsteady","30028"],["stupid-crazy-both","34"],["weird-photos-2013","14"],["when-boys-2","6"],["when-drunk-wasted","5307"],["when-drunk-wasted?epic=mu","26"],["worst-parenting-fails-2","111"],["wtf-photos-2","6312"],["wtf-photos-2?epic=mu","24"],["wtf-photos-2?epic=sf","7"],["wtf-pics","53"],["wtf-pregnancy-pics","67"],["you-rock","7"]]

    puts premium_visits.length
    puts total_visits.length 

    date = DateTime.now.prev_day

    # rake_helper(total_visits,premium_visits,date)

  end

  let(:user) { FactoryGirl.create(:user) }

  scenario "when signed in" do
    as_user(user).visit visits_path
    expect(page).to have_content("akdjaskd")
  end

  scenario "when signed out" do
  	as_visitor(user).visit visits_path
  	expect(page).to have_content('You need to sign in')

  end

end
