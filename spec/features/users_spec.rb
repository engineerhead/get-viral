require 'spec_helper'

feature "Normal User's Access to Users' CRUD " do
  	
    let(:user) { FactoryGirl.create(:user) }

    scenario "when signed in" do
      as_user(user).visit users_path
      expect(page).to have_content('Not authorized as an administrator')
    end

    scenario "when signed out" do
    	as_visitor(user).visit users_path
    	expect(page).to have_content('You need to sign in')
    end

end

feature "Admin's Access to Users' CRUD " do

    background(:all) do
      FactoryGirl.create_list(:user, 3)
    end
    
    let(:admin) { FactoryGirl.create(:user, admin: true) }

    scenario "when signed in" do
      as_user(admin).visit users_path
      expect(page).to have_selector('tr', count: 3)      
    end

    scenario "adding a new user" do
      as_user(admin).visit new_user_path
      expect(page).to have_selector('form')
    end

end
