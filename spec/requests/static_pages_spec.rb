require 'spec_helper'

describe "StaticPages" do
  describe "Home page" do

    it "should have the title home" do
      visit new_user_session_path

      fill_in('Email', with: 'test@example.com')
      fill_in('Password', with: 'randompassword')

      click_button('Sign in')
      expect(page).to have_content('Success')
    end
  end
end
