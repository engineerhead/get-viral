# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#   @user = User.new( 
#         email: 'engineerhead@gmail.com',
#         name: 'Umair Bussi',
#         password: 'Qazxswe12#', 
#         password_confirmation: 'Qazxswe12#', 
#         admin: true,
#         confirmed_at: Time.now
#       )

    @user.skip_confirmation!
    @user.confirm!
    @user.save

    premium_visits = [["1","sm","58241"],["AKS","MU-L","25219"],["AS","MU-L","17469"],["AS2","MU-L","88241"],["AVI","MU-L","38145"],["AW","MU-L","53416"],["Adnan","MU-L","25421"],["Amjad","MU-L","11974"],["BAB","MU-L","22758"],["Bilal","MU-L","5648"],["Charming","MU-L","2953"],["Code","MU-L","8101"],["Danish","MU-L","16222"],["Faisal","MU-L","1178"],["Farooq","MU-L","14588"],["HM","HM","13882"],["HZ","MU-L","9267"],["Haroon","MU-L","26926"],["Haseeb","PW-L","12733"],["Hassan","MU-L","1440"],["Inam","MU-L","4321"],["Irfan","MU-L","12390"],["Irfan2","MU-L","14738"],["JAM","MU-L","6806"],["Jamil","MU-L","6685"],["Jazi","MU-L","9420"],["Jazi","MU-S","13043"],["Middle","MU-L","23016"],["Mubeen","PW-L","9202"],["Mushy","MU-L","331"],["Nomi","MU-L","6988"],["PW","PW-L","8985"],["RM","RM","9029"],["Rani","MU-L","8170"],["SD","SD","121"],["SDA","MU-L","8617"],["SDA2","MU-L","11345"],["Salman","MU-L","1045"],["Salman1","MU-L","74386"],["Salman10","MU-L","5862"],["Salman11","MU-L","2017"],["Salman12","MU-L","3078"],["Salman13","MU-L","863"],["Salman14","MU-L","3510"],["Salman2","MU-L","35188"],["Salman3","MU-L","9053"],["Salman4","MU-L","9626"],["Salman5","MU-L","17300"],["Salman6","MU-L","35228"],["Salman7","MU-L","4882"],["Salman8","MU-L","15537"],["Salman9","MU-L","2743"],["Sarim","MU-L","46311"],["Sheyan","MU-L","13382"],["Sony","PW-L","31432"],["Sony","PW-S","424"],["Spice","MU-L","62271"],["UK","MU-L","9715"],["Wajahat","MU-L","2066"],["Zaheer","MU-L","25453"],["Zahid","MU-L","11591"],["Zia","MU-L","8605"],["celebrities-photoshop","rss","3978"],["celebs-hanging-out","rss","3187"],["celebs-stuck-in-time","rss","2691"],["dc","dc","315"],["ek","ek","173"],["epic-dating-fails","rss","5878"],["food-tattoos","rss","3223"],["gym-fails","rss","4878"],["hs","hs","26748"],["iconic-photos-colorized","rss","3574"],["rehan","MU-L","859"],["relationship-totally-love","rss","4797"],["slownsteady","MU-L","40679"],["when-drunk-wasted","rss","4268"],["worst-parenting-fails-2","rss","4131"],["wtf-photos-2","rss","4159"],["wtf-pregnancy-pics","rss","3345"]]
    total_visits = [["1","93070"],["99","106"],["AKS","29765"],["AS","26955"],["AS2","147372"],["AVI","43319"],["AW","64410"],["Adnan","30177"],["Amjad","17834"],["BAB","24082"],["Bilal","11705"],["Charming","3308"],["Code","10274"],["Danish","24030"],["ES","125"],["Faisal","2522"],["Farooq","17660"],["HM","14861"],["HZ","12883"],["Haroon","33868"],["Haseeb","15914"],["Hassan","4709"],["Inam","4494"],["Irfan","22880"],["Irfan2","79634"],["JAM","7753"],["Jamil","6944"],["Jazi","26887"],["Middle","24778"],["Mubeen","14746"],["Mushy","584"],["Nomi","9474"],["PW","10383"],["RM","19574"],["Rani","18966"],["SD","179"],["SDA","9660"],["SDA2","12263"],["Salman","2447"],["Salman1","91793"],["Salman10","7526"],["Salman11","3342"],["Salman12","8540"],["Salman13","2227"],["Salman14","5536"],["Salman2","42285"],["Salman3","10622"],["Salman4","12141"],["Salman5","17393"],["Salman6","41205"],["Salman7","5993"],["Salman8","24373"],["Salman9","4273"],["Sarim","66509"],["Sheyan","17360"],["Sony","39132"],["Spice","106418"],["UK","10195"],["Wajahat","3212"],["Zaheer","31068"],["Zahid","12648"],["Zia","10603"],["celebrities-photoshop","6875"],["celebs-hanging-out","5100"],["celebs-stuck-in-time","4419"],["dc","378"],["ek","180"],["epic-dating-fails","9151"],["food-tattoos","4556"],["gym-fails","8110"],["hs","54681"],["iconic-photos-colorized","5298"],["rehan","1290"],["relationship-totally-love","8757"],["slownsteady","49853"],["when-drunk-wasted","6878"],["worst-parenting-fails-2","5685"],["wtf-photos-2","7286"],["wtf-pregnancy-pics","5565"]]

    @premium_totals = Hash.new(0)

    premium_visits.each do |visit|
      @premium_totals[visit.first] += visit.last.to_i
    end

    # keys_col = @premium_totals.keys

    # keys_col.each do |e|
    #   rand_string = SecureRandom.hex(3)
    #   @user = User.new( 
    #     name: e,
    #     email: rand_string+'@test.com' ,
    #     password: 'changeme',
    #     password_confirmation: 'changeme',
    #     profile_attributes: { level: 'Silver', rate: Random.rand(2.5..5.5).round(2), source: e, campaign: e, referral_id: 2}
    #   )

    #   @user.skip_confirmation!
    #   @user.confirm!
    #   @user.save
    # end

    @users = User.includes(:profile).where(admin: false)


    @premium_totals.each do |source,value|
      p_visits = value.to_i 
      
      user = @users.find {|u| u.profile.source == source}

      unless user.nil?

        rate = user.profile.rate
        u_revenue = (p_visits/1000) * rate

        related_total = total_visits.find { |t| t.first == source }
        t_visits = related_total.last
        
        user.visits.create( total: t_visits, premium: p_visits, revenue: u_revenue ) 
      end
    end
  