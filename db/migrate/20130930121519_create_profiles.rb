class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :level
      t.float :rate
      t.string :campaign
      t.string :source
      t.integer :referral_id
      t.integer :user_id

      t.timestamps
    end
  end
end
