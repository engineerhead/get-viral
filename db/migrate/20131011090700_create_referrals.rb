class CreateReferrals < ActiveRecord::Migration
  def change
    create_table :referrals do |t|
      t.integer :user_id
      t.string :campaign
      t.string :source
      t.float :revenue

      t.timestamps
    end
  end
end
