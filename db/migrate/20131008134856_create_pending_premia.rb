class CreatePendingPremia < ActiveRecord::Migration
  def change
    create_table :pending_premia do |t|
      t.string :campaign
      t.string :source
      t.string :visits

      t.timestamps
    end
  end
end
