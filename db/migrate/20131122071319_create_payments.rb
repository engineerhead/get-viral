class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :user_id
      t.float :amount
      t.string :period
      t.boolean :paid
      t.date :paid_on

      t.timestamps
    end
  end
end
