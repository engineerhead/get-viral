class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.string :total
      t.string :premium

      t.timestamps
    end
  end
end
