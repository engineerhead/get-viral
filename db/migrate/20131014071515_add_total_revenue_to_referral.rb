class AddTotalRevenueToReferral < ActiveRecord::Migration
  def change
    add_column :referrals, :totalrevenue, :float
  end
end
