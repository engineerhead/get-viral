class AddRevenueToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :revenue, :float
  end
end
