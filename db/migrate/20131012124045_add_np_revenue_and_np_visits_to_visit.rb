class AddNpRevenueAndNpVisitsToVisit < ActiveRecord::Migration
  def change
    add_column :visits, :npvisits, :string
    add_column :visits, :nprevenue, :float
  end
end
