class AddTotalRevenueToVisit < ActiveRecord::Migration
  def change
    add_column :visits, :totalrevenue, :float
  end
end
