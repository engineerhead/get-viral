class AddNpVisitsAndNpRevenueToReferral < ActiveRecord::Migration
  def change
    add_column :referrals, :npvisits, :string
    add_column :referrals, :nprevenue, :float
  end
end
