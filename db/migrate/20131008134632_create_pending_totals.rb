class CreatePendingTotals < ActiveRecord::Migration
  def change
    create_table :pending_totals do |t|
      t.string :campaign
      t.string :visits

      t.timestamps
    end
  end
end
