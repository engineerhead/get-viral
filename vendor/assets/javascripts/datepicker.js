(function(d) {
  function s() {
    return new Date(Date.UTC.apply(Date, arguments))
  }
  function w(a, b) {
    var c = d(a).data(), h = {}, e, g = RegExp("^" + b.toLowerCase() + "([A-Z])");
    b = RegExp("^" + b.toLowerCase());
    for(var f in c) {
      b.test(f) && (e = f.replace(g, function(a, b) {
        return b.toLowerCase()
      }), h[e] = c[f])
    }
    return h
  }
  function x(a) {
    var b = {};
    if(!n[a] && (a = a.split("-")[0], !n[a])) {
      return
    }
    var c = n[a];
    d.each(y, function(a, d) {
      d in c && (b[d] = c[d])
    });
    return b
  }
  var t = d(window), r = function(a, b) {
    this._process_options(b);
    this.element = d(a);
    this.isInline = !1;
    this.isInput = this.element.is("input");
    this.hasInput = (this.component = this.element.is(".date") ? this.element.find(".add-on, .btn") : !1) && this.element.find("input").length;
    this.component && 0 === this.component.length && (this.component = !1);
    this.picker = d(m.template);
    this._buildEvents();
    this._attachEvents();
    this.isInline ? this.picker.addClass("datepicker-inline").appendTo(this.element) : this.picker.addClass("datepicker-dropdown dropdown-menu");
    this.o.rtl && (this.picker.addClass("datepicker-rtl"), this.picker.find(".prev i, .next i").toggleClass("icon-arrow-left icon-arrow-right"));
    this.viewMode = this.o.startView;
    this.o.calendarWeeks && this.picker.find("tfoot th.today").attr("colspan", function(a, b) {
      return parseInt(b) + 1
    });
    this._allow_update = !1;
    this.setStartDate(this._o.startDate);
    this.setEndDate(this._o.endDate);
    this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled);
    this.fillDow();
    this.fillMonths();
    this._allow_update = !0;
    this.update();
    this.showMode();
    this.isInline && this.show()
  };
  r.prototype = {constructor:r, _process_options:function(a) {
    this._o = d.extend({}, this._o, a);
    a = this.o = d.extend({}, this._o);
    var b = a.language;
    n[b] || (b = b.split("-")[0], n[b] || (b = u.language));
    a.language = b;
    switch(a.startView) {
      case 2:
      ;
      case "decade":
        a.startView = 2;
        break;
      case 1:
      ;
      case "year":
        a.startView = 1;
        break;
      default:
        a.startView = 0
    }
    switch(a.minViewMode) {
      case 1:
      ;
      case "months":
        a.minViewMode = 1;
        break;
      case 2:
      ;
      case "years":
        a.minViewMode = 2;
        break;
      default:
        a.minViewMode = 0
    }
    a.startView = Math.max(a.startView, a.minViewMode);
    a.weekStart %= 7;
    a.weekEnd = (a.weekStart + 6) % 7;
    b = m.parseFormat(a.format);
    -Infinity !== a.startDate && (a.startDate = a.startDate ? a.startDate instanceof Date ? this._local_to_utc(this._zero_time(a.startDate)) : m.parseDate(a.startDate, b, a.language) : -Infinity);
    Infinity !== a.endDate && (a.endDate = a.endDate ? a.endDate instanceof Date ? this._local_to_utc(this._zero_time(a.endDate)) : m.parseDate(a.endDate, b, a.language) : Infinity);
    a.daysOfWeekDisabled = a.daysOfWeekDisabled || [];
    d.isArray(a.daysOfWeekDisabled) || (a.daysOfWeekDisabled = a.daysOfWeekDisabled.split(/[,\s]*/));
    a.daysOfWeekDisabled = d.map(a.daysOfWeekDisabled, function(a) {
      return parseInt(a, 10)
    });
    var b = String(a.orientation).toLowerCase().split(/\s+/g), c = a.orientation.toLowerCase(), b = d.grep(b, function(a) {
      return/^auto|left|right|top|bottom$/.test(a)
    });
    a.orientation = {x:"auto", y:"auto"};
    if(c && "auto" !== c) {
      if(1 === b.length) {
        switch(b[0]) {
          case "top":
          ;
          case "bottom":
            a.orientation.y = b[0];
            break;
          case "left":
          ;
          case "right":
            a.orientation.x = b[0]
        }
      }else {
        c = d.grep(b, function(a) {
          return/^left|right$/.test(a)
        }), a.orientation.x = c[0] || "auto", c = d.grep(b, function(a) {
          return/^top|bottom$/.test(a)
        }), a.orientation.y = c[0] || "auto"
      }
    }
  }, _events:[], _secondaryEvents:[], _applyEvents:function(a) {
    for(var b = 0, c, d;b < a.length;b++) {
      c = a[b][0], d = a[b][1], c.on(d)
    }
  }, _unapplyEvents:function(a) {
    for(var b = 0, c, d;b < a.length;b++) {
      c = a[b][0], d = a[b][1], c.off(d)
    }
  }, _buildEvents:function() {
    this.isInput ? this._events = [[this.element, {focus:d.proxy(this.show, this), keyup:d.proxy(this.update, this), keydown:d.proxy(this.keydown, this)}]] : this.component && this.hasInput ? this._events = [[this.element.find("input"), {focus:d.proxy(this.show, this), keyup:d.proxy(this.update, this), keydown:d.proxy(this.keydown, this)}], [this.component, {click:d.proxy(this.show, this)}]] : this.element.is("div") ? this.isInline = !0 : this._events = [[this.element, {click:d.proxy(this.show, this)}]];
    this._secondaryEvents = [[this.picker, {click:d.proxy(this.click, this)}], [d(window), {resize:d.proxy(this.place, this)}], [d(document), {"mousedown touchstart":d.proxy(function(a) {
      this.element.is(a.target) || this.element.find(a.target).length || this.picker.is(a.target) || this.picker.find(a.target).length || this.hide()
    }, this)}]]
  }, _attachEvents:function() {
    this._detachEvents();
    this._applyEvents(this._events)
  }, _detachEvents:function() {
    this._unapplyEvents(this._events)
  }, _attachSecondaryEvents:function() {
    this._detachSecondaryEvents();
    this._applyEvents(this._secondaryEvents)
  }, _detachSecondaryEvents:function() {
    this._unapplyEvents(this._secondaryEvents)
  }, _trigger:function(a, b) {
    var c = b || this.date, h = this._utc_to_local(c);
    this.element.trigger({type:a, date:h, format:d.proxy(function(a) {
      return m.formatDate(c, a || this.o.format, this.o.language)
    }, this)})
  }, show:function(a) {
    this.isInline || this.picker.appendTo("body");
    this.picker.show();
    this.height = this.component ? this.component.outerHeight() : this.element.outerHeight();
    this.place();
    this._attachSecondaryEvents();
    a && a.preventDefault();
    this._trigger("show")
  }, hide:function(a) {
    !this.isInline && this.picker.is(":visible") && (this.picker.hide().detach(), this._detachSecondaryEvents(), this.viewMode = this.o.startView, this.showMode(), this.o.forceParse && (this.isInput && this.element.val() || this.hasInput && this.element.find("input").val()) && this.setValue(), this._trigger("hide"))
  }, remove:function() {
    this.hide();
    this._detachEvents();
    this._detachSecondaryEvents();
    this.picker.remove();
    delete this.element.data().datepicker;
    this.isInput || delete this.element.data().date
  }, _utc_to_local:function(a) {
    return new Date(a.getTime() + 6E4 * a.getTimezoneOffset())
  }, _local_to_utc:function(a) {
    return new Date(a.getTime() - 6E4 * a.getTimezoneOffset())
  }, _zero_time:function(a) {
    return new Date(a.getFullYear(), a.getMonth(), a.getDate())
  }, _zero_utc_time:function(a) {
    return new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate()))
  }, getDate:function() {
    return this._utc_to_local(this.getUTCDate())
  }, getUTCDate:function() {
    return this.date
  }, setDate:function(a) {
    this.setUTCDate(this._local_to_utc(a))
  }, setUTCDate:function(a) {
    this.date = a;
    this.setValue()
  }, setValue:function() {
    var a = this.getFormattedDate();
    this.isInput ? this.element.val(a).change() : this.component && this.element.find("input").val(a).change()
  }, getFormattedDate:function(a) {
    void 0 === a && (a = this.o.format);
    return m.formatDate(this.date, a, this.o.language)
  }, setStartDate:function(a) {
    this._process_options({startDate:a});
    this.update();
    this.updateNavArrows()
  }, setEndDate:function(a) {
    this._process_options({endDate:a});
    this.update();
    this.updateNavArrows()
  }, setDaysOfWeekDisabled:function(a) {
    this._process_options({daysOfWeekDisabled:a});
    this.update();
    this.updateNavArrows()
  }, place:function() {
    if(!this.isInline) {
      var a = this.picker.outerWidth(), b = this.picker.outerHeight(), c = t.width(), h = t.height(), e = t.scrollTop(), g = parseInt(this.element.parents().filter(function() {
        return"auto" != d(this).css("z-index")
      }).first().css("z-index")) + 10, f = this.component ? this.component.parent().offset() : this.element.offset(), l = this.component ? this.component.outerHeight(!0) : this.element.outerHeight(!1), m = this.component ? this.component.outerWidth(!0) : this.element.outerWidth(!1), k = f.left, n = f.top;
      this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left");
      "auto" !== this.o.orientation.x ? (this.picker.addClass("datepicker-orient-" + this.o.orientation.x), "right" === this.o.orientation.x && (k -= a - m)) : (this.picker.addClass("datepicker-orient-left"), 0 > f.left ? k -= f.left - 10 : f.left + a > c && (k = c - a - 10));
      a = this.o.orientation.y;
      "auto" === a && (a = -e + f.top - b, h = e + h - (f.top + l + b), a = Math.max(a, h) === h ? "top" : "bottom");
      this.picker.addClass("datepicker-orient-" + a);
      n = "top" === a ? n + l : n - (b + parseInt(this.picker.css("padding-top")));
      this.picker.css({top:n, left:k, zIndex:g})
    }
  }, _allow_update:!0, update:function() {
    if(this._allow_update) {
      var a = new Date(this.date), b, c = !1;
      arguments && arguments.length && ("string" === typeof arguments[0] || arguments[0] instanceof Date) ? (b = arguments[0], b instanceof Date && (b = this._local_to_utc(b)), c = !0) : (b = this.isInput ? this.element.val() : this.element.data("date") || this.element.find("input").val(), delete this.element.data().date);
      this.date = m.parseDate(b, this.o.format, this.o.language);
      c ? this.setValue() : b ? a.getTime() !== this.date.getTime() && this._trigger("changeDate") : this._trigger("clearDate");
      this.date < this.o.startDate ? (this.viewDate = new Date(this.o.startDate), this.date = new Date(this.o.startDate)) : this.date > this.o.endDate ? (this.viewDate = new Date(this.o.endDate), this.date = new Date(this.o.endDate)) : (this.viewDate = new Date(this.date), this.date = new Date(this.date));
      this.fill()
    }
  }, fillDow:function() {
    var a = this.o.weekStart, b = "<tr>";
    this.o.calendarWeeks && (b += '<th class="cw">&nbsp;</th>', this.picker.find(".datepicker-days thead tr:first-child").prepend('<th class="cw">&nbsp;</th>'));
    for(;a < this.o.weekStart + 7;) {
      b += '<th class="dow">' + n[this.o.language].daysMin[a++ % 7] + "</th>"
    }
    b += "</tr>";
    this.picker.find(".datepicker-days thead").append(b)
  }, fillMonths:function() {
    for(var a = "", b = 0;12 > b;) {
      a += '<span class="month">' + n[this.o.language].monthsShort[b++] + "</span>"
    }
    this.picker.find(".datepicker-months td").html(a)
  }, setRange:function(a) {
    a && a.length ? this.range = d.map(a, function(a) {
      return a.valueOf()
    }) : delete this.range;
    this.fill()
  }, getClassNames:function(a) {
    var b = [], c = this.viewDate.getUTCFullYear(), h = this.viewDate.getUTCMonth(), e = this.date.valueOf(), g = new Date;
    a.getUTCFullYear() < c || a.getUTCFullYear() == c && a.getUTCMonth() < h ? b.push("old") : (a.getUTCFullYear() > c || a.getUTCFullYear() == c && a.getUTCMonth() > h) && b.push("new");
    this.o.todayHighlight && a.getUTCFullYear() == g.getFullYear() && a.getUTCMonth() == g.getMonth() && a.getUTCDate() == g.getDate() && b.push("today");
    a.valueOf() == e && b.push("active");
    (a.valueOf() < this.o.startDate || a.valueOf() > this.o.endDate || -1 !== d.inArray(a.getUTCDay(), this.o.daysOfWeekDisabled)) && b.push("disabled");
    this.range && (a > this.range[0] && a < this.range[this.range.length - 1] && b.push("range"), -1 != d.inArray(a.valueOf(), this.range) && b.push("selected"));
    return b
  }, fill:function() {
    var a = new Date(this.viewDate), b = a.getUTCFullYear(), c = a.getUTCMonth(), a = -Infinity !== this.o.startDate ? this.o.startDate.getUTCFullYear() : -Infinity, h = -Infinity !== this.o.startDate ? this.o.startDate.getUTCMonth() : -Infinity, e = Infinity !== this.o.endDate ? this.o.endDate.getUTCFullYear() : Infinity, g = Infinity !== this.o.endDate ? this.o.endDate.getUTCMonth() : Infinity, f;
    this.picker.find(".datepicker-days thead th.datepicker-switch").text(n[this.o.language].months[c] + " " + b);
    this.picker.find("tfoot th.today").text(n[this.o.language].today).toggle(!1 !== this.o.todayBtn);
    this.picker.find("tfoot th.clear").text(n[this.o.language].clear).toggle(!1 !== this.o.clearBtn);
    this.updateNavArrows();
    this.fillMonths();
    var l = s(b, c - 1, 28, 0, 0, 0, 0), c = m.getDaysInMonth(l.getUTCFullYear(), l.getUTCMonth());
    l.setUTCDate(c);
    l.setUTCDate(c - (l.getUTCDay() - this.o.weekStart + 7) % 7);
    var p = new Date(l);
    p.setUTCDate(p.getUTCDate() + 42);
    for(var p = p.valueOf(), c = [], k;l.valueOf() < p;) {
      if(l.getUTCDay() == this.o.weekStart && (c.push("<tr>"), this.o.calendarWeeks)) {
        k = new Date(+l + 864E5 * ((this.o.weekStart - l.getUTCDay() - 7) % 7));
        k = new Date(+k + 864E5 * ((11 - k.getUTCDay()) % 7));
        var r = new Date(+(r = s(k.getUTCFullYear(), 0, 1)) + 864E5 * ((11 - r.getUTCDay()) % 7));
        c.push('<td class="cw">' + ((k - r) / 864E5 / 7 + 1) + "</td>")
      }
      k = this.getClassNames(l);
      k.push("day");
      if(this.o.beforeShowDay !== d.noop) {
        var q = this.o.beforeShowDay(this._utc_to_local(l));
        void 0 === q ? q = {} : "boolean" === typeof q ? q = {enabled:q} : "string" === typeof q && (q = {classes:q});
        !1 === q.enabled && k.push("disabled");
        q.classes && (k = k.concat(q.classes.split(/\s+/)));
        q.tooltip && (f = q.tooltip)
      }
      k = d.unique(k);
      c.push('<td class="' + k.join(" ") + '"' + (f ? ' title="' + f + '"' : "") + ">" + l.getUTCDate() + "</td>");
      l.getUTCDay() == this.o.weekEnd && c.push("</tr>");
      l.setUTCDate(l.getUTCDate() + 1)
    }
    this.picker.find(".datepicker-days tbody").empty().append(c.join(""));
    f = this.date && this.date.getUTCFullYear();
    c = this.picker.find(".datepicker-months").find("th:eq(1)").text(b).end().find("span").removeClass("active");
    f && f == b && c.eq(this.date.getUTCMonth()).addClass("active");
    (b < a || b > e) && c.addClass("disabled");
    b == a && c.slice(0, h).addClass("disabled");
    b == e && c.slice(g + 1).addClass("disabled");
    c = "";
    b = 10 * parseInt(b / 10, 10);
    h = this.picker.find(".datepicker-years").find("th:eq(1)").text(b + "-" + (b + 9)).end().find("td");
    b -= 1;
    for(g = -1;11 > g;g++) {
      c += '<span class="year' + (-1 == g ? " old" : 10 == g ? " new" : "") + (f == b ? " active" : "") + (b < a || b > e ? " disabled" : "") + '">' + b + "</span>", b += 1
    }
    h.html(c)
  }, updateNavArrows:function() {
    if(this._allow_update) {
      var a = new Date(this.viewDate), b = a.getUTCFullYear(), a = a.getUTCMonth();
      switch(this.viewMode) {
        case 0:
          -Infinity !== this.o.startDate && b <= this.o.startDate.getUTCFullYear() && a <= this.o.startDate.getUTCMonth() ? this.picker.find(".prev").css({visibility:"hidden"}) : this.picker.find(".prev").css({visibility:"visible"});
          Infinity !== this.o.endDate && b >= this.o.endDate.getUTCFullYear() && a >= this.o.endDate.getUTCMonth() ? this.picker.find(".next").css({visibility:"hidden"}) : this.picker.find(".next").css({visibility:"visible"});
          break;
        case 1:
        ;
        case 2:
          -Infinity !== this.o.startDate && b <= this.o.startDate.getUTCFullYear() ? this.picker.find(".prev").css({visibility:"hidden"}) : this.picker.find(".prev").css({visibility:"visible"}), Infinity !== this.o.endDate && b >= this.o.endDate.getUTCFullYear() ? this.picker.find(".next").css({visibility:"hidden"}) : this.picker.find(".next").css({visibility:"visible"})
      }
    }
  }, click:function(a) {
    a.preventDefault();
    a = d(a.target).closest("span, td, th");
    if(1 == a.length) {
      switch(a[0].nodeName.toLowerCase()) {
        case "th":
          switch(a[0].className) {
            case "datepicker-switch":
              this.showMode(1);
              break;
            case "prev":
            ;
            case "next":
              a = m.modes[this.viewMode].navStep * ("prev" == a[0].className ? -1 : 1);
              switch(this.viewMode) {
                case 0:
                  this.viewDate = this.moveMonth(this.viewDate, a);
                  this._trigger("changeMonth", this.viewDate);
                  break;
                case 1:
                ;
                case 2:
                  this.viewDate = this.moveYear(this.viewDate, a), 1 === this.viewMode && this._trigger("changeYear", this.viewDate)
              }
              this.fill();
              break;
            case "today":
              a = new Date;
              a = s(a.getFullYear(), a.getMonth(), a.getDate(), 0, 0, 0);
              this.showMode(-2);
              this._setDate(a, "linked" == this.o.todayBtn ? null : "view");
              break;
            case "clear":
              var b;
              this.isInput ? b = this.element : this.component && (b = this.element.find("input"));
              b && b.val("").change();
              this._trigger("changeDate");
              this.update();
              this.o.autoclose && this.hide()
          }
          break;
        case "span":
          if(!a.is(".disabled")) {
            this.viewDate.setUTCDate(1);
            if(a.is(".month")) {
              b = 1;
              var c = a.parent().find("span").index(a), h = this.viewDate.getUTCFullYear();
              this.viewDate.setUTCMonth(c);
              this._trigger("changeMonth", this.viewDate);
              1 === this.o.minViewMode && this._setDate(s(h, c, b, 0, 0, 0, 0))
            }else {
              h = parseInt(a.text(), 10) || 0, b = 1, c = 0, this.viewDate.setUTCFullYear(h), this._trigger("changeYear", this.viewDate), 2 === this.o.minViewMode && this._setDate(s(h, c, b, 0, 0, 0, 0))
            }
            this.showMode(-1);
            this.fill()
          }
          break;
        case "td":
          a.is(".day") && !a.is(".disabled") && (b = parseInt(a.text(), 10) || 1, h = this.viewDate.getUTCFullYear(), c = this.viewDate.getUTCMonth(), a.is(".old") ? 0 === c ? (c = 11, h -= 1) : c -= 1 : a.is(".new") && (11 == c ? (c = 0, h += 1) : c += 1), this._setDate(s(h, c, b, 0, 0, 0, 0)))
      }
    }
  }, _setDate:function(a, b) {
    b && "date" != b || (this.date = new Date(a));
    b && "view" != b || (this.viewDate = new Date(a));
    this.fill();
    this.setValue();
    this._trigger("changeDate");
    var c;
    this.isInput ? c = this.element : this.component && (c = this.element.find("input"));
    c && c.change();
    !this.o.autoclose || b && "date" != b || this.hide()
  }, moveMonth:function(a, b) {
    if(!b) {
      return a
    }
    var c = new Date(a.valueOf()), d = c.getUTCDate(), e = c.getUTCMonth(), g = Math.abs(b), f;
    b = 0 < b ? 1 : -1;
    if(1 == g) {
      if(g = -1 == b ? function() {
        return c.getUTCMonth() == e
      } : function() {
        return c.getUTCMonth() != f
      }, f = e + b, c.setUTCMonth(f), 0 > f || 11 < f) {
        f = (f + 12) % 12
      }
    }else {
      for(var l = 0;l < g;l++) {
        c = this.moveMonth(c, b)
      }
      f = c.getUTCMonth();
      c.setUTCDate(d);
      g = function() {
        return f != c.getUTCMonth()
      }
    }
    for(;g();) {
      c.setUTCDate(--d), c.setUTCMonth(f)
    }
    return c
  }, moveYear:function(a, b) {
    return this.moveMonth(a, 12 * b)
  }, dateWithinRange:function(a) {
    return a >= this.o.startDate && a <= this.o.endDate
  }, keydown:function(a) {
    if(this.picker.is(":not(:visible)")) {
      27 == a.keyCode && this.show()
    }else {
      var b = !1, c, d, e;
      switch(a.keyCode) {
        case 27:
          this.hide();
          a.preventDefault();
          break;
        case 37:
        ;
        case 39:
          if(!this.o.keyboardNavigation) {
            break
          }
          c = 37 == a.keyCode ? -1 : 1;
          a.ctrlKey ? (d = this.moveYear(this.date, c), e = this.moveYear(this.viewDate, c), this._trigger("changeYear", this.viewDate)) : a.shiftKey ? (d = this.moveMonth(this.date, c), e = this.moveMonth(this.viewDate, c), this._trigger("changeMonth", this.viewDate)) : (d = new Date(this.date), d.setUTCDate(this.date.getUTCDate() + c), e = new Date(this.viewDate), e.setUTCDate(this.viewDate.getUTCDate() + c));
          this.dateWithinRange(d) && (this.date = d, this.viewDate = e, this.setValue(), this.update(), a.preventDefault(), b = !0);
          break;
        case 38:
        ;
        case 40:
          if(!this.o.keyboardNavigation) {
            break
          }
          c = 38 == a.keyCode ? -1 : 1;
          a.ctrlKey ? (d = this.moveYear(this.date, c), e = this.moveYear(this.viewDate, c), this._trigger("changeYear", this.viewDate)) : a.shiftKey ? (d = this.moveMonth(this.date, c), e = this.moveMonth(this.viewDate, c), this._trigger("changeMonth", this.viewDate)) : (d = new Date(this.date), d.setUTCDate(this.date.getUTCDate() + 7 * c), e = new Date(this.viewDate), e.setUTCDate(this.viewDate.getUTCDate() + 7 * c));
          this.dateWithinRange(d) && (this.date = d, this.viewDate = e, this.setValue(), this.update(), a.preventDefault(), b = !0);
          break;
        case 13:
          this.hide();
          a.preventDefault();
          break;
        case 9:
          this.hide()
      }
      if(b) {
        this._trigger("changeDate");
        var g;
        this.isInput ? g = this.element : this.component && (g = this.element.find("input"));
        g && g.change()
      }
    }
  }, showMode:function(a) {
    a && (this.viewMode = Math.max(this.o.minViewMode, Math.min(2, this.viewMode + a)));
    this.picker.find(">div").hide().filter(".datepicker-" + m.modes[this.viewMode].clsName).css("display", "block");
    this.updateNavArrows()
  }};
  var v = function(a, b) {
    this.element = d(a);
    this.inputs = d.map(b.inputs, function(a) {
      return a.jquery ? a[0] : a
    });
    delete b.inputs;
    d(this.inputs).datepicker(b).bind("changeDate", d.proxy(this.dateUpdated, this));
    this.pickers = d.map(this.inputs, function(a) {
      return d(a).data("datepicker")
    });
    this.updateDates()
  };
  v.prototype = {updateDates:function() {
    this.dates = d.map(this.pickers, function(a) {
      return a.date
    });
    this.updateRanges()
  }, updateRanges:function() {
    var a = d.map(this.dates, function(a) {
      return a.valueOf()
    });
    d.each(this.pickers, function(b, c) {
      c.setRange(a)
    })
  }, dateUpdated:function(a) {
    var b = d(a.target).data("datepicker").getUTCDate();
    a = d.inArray(a.target, this.inputs);
    var c = this.inputs.length;
    if(-1 != a) {
      if(b < this.dates[a]) {
        for(;0 <= a && b < this.dates[a];) {
          this.pickers[a--].setUTCDate(b)
        }
      }else {
        if(b > this.dates[a]) {
          for(;a < c && b > this.dates[a];) {
            this.pickers[a++].setUTCDate(b)
          }
        }
      }
      this.updateDates()
    }
  }, remove:function() {
    d.map(this.pickers, function(a) {
      a.remove()
    });
    delete this.element.data().datepicker
  }};
  var z = d.fn.datepicker;
  d.fn.datepicker = function(a) {
    var b = Array.apply(null, arguments);
    b.shift();
    var c;
    this.each(function() {
      var h = d(this), e = h.data("datepicker"), g = "object" == typeof a && a;
      if(!e) {
        var e = w(this, "date"), f = d.extend({}, u, e, g), f = x(f.language), g = d.extend({}, u, f, e, g);
        h.is(".input-daterange") || g.inputs ? (e = {inputs:g.inputs || h.find("input").toArray()}, h.data("datepicker", e = new v(this, d.extend(g, e)))) : h.data("datepicker", e = new r(this, g))
      }
      if("string" == typeof a && "function" == typeof e[a] && (c = e[a].apply(e, b), void 0 !== c)) {
        return!1
      }
    });
    return void 0 !== c ? c : this
  };
  var u = d.fn.datepicker.defaults = {autoclose:!1, beforeShowDay:d.noop, calendarWeeks:!1, clearBtn:!1, daysOfWeekDisabled:[], endDate:Infinity, forceParse:!0, format:"mm/dd/yyyy", keyboardNavigation:!0, language:"en", minViewMode:0, orientation:"auto", rtl:!1, startDate:-Infinity, startView:0, todayBtn:!1, todayHighlight:!1, weekStart:0}, y = d.fn.datepicker.locale_opts = ["format", "rtl", "weekStart"];
  d.fn.datepicker.Constructor = r;
  var n = d.fn.datepicker.dates = {en:{days:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday Sunday".split(" "), daysShort:"Sun Mon Tue Wed Thu Fri Sat Sun".split(" "), daysMin:"Su Mo Tu We Th Fr Sa Su".split(" "), months:"January February March April May June July August September October November December".split(" "), monthsShort:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "), today:"Today", clear:"Clear"}}, m = {modes:[{clsName:"days", navFnc:"Month", navStep:1}, {clsName:"months", 
  navFnc:"FullYear", navStep:1}, {clsName:"years", navFnc:"FullYear", navStep:10}], isLeapYear:function(a) {
    return 0 === a % 4 && 0 !== a % 100 || 0 === a % 400
  }, getDaysInMonth:function(a, b) {
    return[31, m.isLeapYear(a) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][b]
  }, validParts:/dd?|DD?|mm?|MM?|yy(?:yy)?/g, nonpunctuation:/[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g, parseFormat:function(a) {
    var b = a.replace(this.validParts, "\x00").split("\x00");
    a = a.match(this.validParts);
    if(!b || !b.length || !a || 0 === a.length) {
      throw Error("Invalid date format.");
    }
    return{separators:b, parts:a}
  }, parseDate:function(a, b, c) {
    if(a instanceof Date) {
      return a
    }
    "string" === typeof b && (b = m.parseFormat(b));
    if(/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(a)) {
      var h = /([\-+]\d+)([dmwy])/, e = a.match(/([\-+]\d+)([dmwy])/g), g;
      a = new Date;
      for(var f = 0;f < e.length;f++) {
        switch(b = h.exec(e[f]), g = parseInt(b[1]), b[2]) {
          case "d":
            a.setUTCDate(a.getUTCDate() + g);
            break;
          case "m":
            a = r.prototype.moveMonth.call(r.prototype, a, g);
            break;
          case "w":
            a.setUTCDate(a.getUTCDate() + 7 * g);
            break;
          case "y":
            a = r.prototype.moveYear.call(r.prototype, a, g)
        }
      }
      return s(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate(), 0, 0, 0)
    }
    e = a && a.match(this.nonpunctuation) || [];
    a = new Date;
    var h = {}, l = "yyyy yy M MM m mm d dd".split(" ");
    g = {yyyy:function(a, b) {
      return a.setUTCFullYear(b)
    }, yy:function(a, b) {
      return a.setUTCFullYear(2E3 + b)
    }, m:function(a, b) {
      if(isNaN(a)) {
        return a
      }
      for(b -= 1;0 > b;) {
        b += 12
      }
      b %= 12;
      for(a.setUTCMonth(b);a.getUTCMonth() != b;) {
        a.setUTCDate(a.getUTCDate() - 1)
      }
      return a
    }, d:function(a, b) {
      return a.setUTCDate(b)
    }};
    var p;
    g.M = g.MM = g.mm = g.m;
    g.dd = g.d;
    a = s(a.getFullYear(), a.getMonth(), a.getDate(), 0, 0, 0);
    var k = b.parts.slice();
    e.length != k.length && (k = d(k).filter(function(a, b) {
      return-1 !== d.inArray(b, l)
    }).toArray());
    if(e.length == k.length) {
      for(var f = 0, t = k.length;f < t;f++) {
        p = parseInt(e[f], 10);
        b = k[f];
        if(isNaN(p)) {
          switch(b) {
            case "MM":
              p = d(n[c].months).filter(function() {
                var a = this.slice(0, e[f].length), b = e[f].slice(0, a.length);
                return a == b
              });
              p = d.inArray(p[0], n[c].months) + 1;
              break;
            case "M":
              p = d(n[c].monthsShort).filter(function() {
                var a = this.slice(0, e[f].length), b = e[f].slice(0, a.length);
                return a == b
              }), p = d.inArray(p[0], n[c].monthsShort) + 1
          }
        }
        h[b] = p
      }
      for(f = 0;f < l.length;f++) {
        c = l[f], c in h && !isNaN(h[c]) && (b = new Date(a), g[c](b, h[c]), isNaN(b) || (a = b))
      }
    }
    return a
  }, formatDate:function(a, b, c) {
    "string" === typeof b && (b = m.parseFormat(b));
    c = {d:a.getUTCDate(), D:n[c].daysShort[a.getUTCDay()], DD:n[c].days[a.getUTCDay()], m:a.getUTCMonth() + 1, M:n[c].monthsShort[a.getUTCMonth()], MM:n[c].months[a.getUTCMonth()], yy:a.getUTCFullYear().toString().substring(2), yyyy:a.getUTCFullYear()};
    c.dd = (10 > c.d ? "0" : "") + c.d;
    c.mm = (10 > c.m ? "0" : "") + c.m;
    a = [];
    for(var h = d.extend([], b.separators), e = 0, g = b.parts.length;e <= g;e++) {
      h.length && a.push(h.shift()), a.push(c[b.parts[e]])
    }
    return a.join("")
  }, headTemplate:'<thead><tr><th class="prev">&laquo;</th><th colspan="5" class="datepicker-switch"></th><th class="next">&raquo;</th></tr></thead>', contTemplate:'<tbody><tr><td colspan="7"></td></tr></tbody>', footTemplate:'<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'};
  m.template = '<div class="datepicker"><div class="datepicker-days"><table class=" table-condensed">' + m.headTemplate + "<tbody></tbody>" + m.footTemplate + '</table></div><div class="datepicker-months"><table class="table-condensed">' + m.headTemplate + m.contTemplate + m.footTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + m.headTemplate + m.contTemplate + m.footTemplate + "</table></div></div>";
  d.fn.datepicker.DPGlobal = m;
  d.fn.datepicker.noConflict = function() {
    d.fn.datepicker = z;
    return this
  };
  d(document).on("focus.datepicker.data-api click.datepicker.data-api", '[data-provide="datepicker"]', function(a) {
    var b = d(this);
    b.data("datepicker") || (a.preventDefault(), b.datepicker("show"))
  });
  d(function() {
    d('[data-provide="datepicker-inline"]').datepicker()
  })
})(window.jQuery);