$(function(){
  processData();
})

// Process data also on page:change due to TurboLinks
$(window).bind('page:change', function() {
  processData();
});

//Updates Tiles and Renders Charts
function processData(){
  //Enable Date Picker on From and To elements
  $( '.date-picker' ).datepicker({ 
    "autoclose": true,
    "format" : "dd-mm-yyyy"
  });

  var user_data =  window.visits_data;
  var referral_data = window.referral_data;

  // Update Tiles with Referral Data
  // if (referral_data && referral_data.length > 0) {
  //   var referral_visits = 0,
  //       referral_revenue = 0;
  //   $.each(referral_data, function( index, item){
  //     referral_visits += parseInt(item.visits) + parseInt(item.npvisits);
  //     referral_revenue += parseFloat(item.totalrevenue);
  //   });

  //   $( '#total-referral-visits h2' ).text( referral_visits );
  //   $( '#referral-revenue h2' ).text( '$' + referral_revenue.toFixed(2) );
  // }

  //Proceed if there is Visits data
  if( user_data && user_data.length > 0 ){
  //   var total = 0,
  //       premium = 0,
  //       nonpremium = 0,
  //       revenue = 0; 

  //   //Calculate Sum of Total and Premium Visits
  //   //also Total Revenue
  //   $.each(user_data, function( index, item){
  //     total += parseInt(item.total);
  //     premium += parseInt(item.premium);
  //     nonpremium += parseInt(item.npvisits);
  //     revenue += parseFloat(item.totalrevenue) ;
  //   });

    //Update Tiles with Relevant Data
    // $( '#total-visits h2' ).text( total );
    // $( '#premium-visits h2' ).text( premium );
    // $( '#revenue h2' ).text( '$' + revenue.toFixed(2) );

    if( $( '#revenue-chart' ).length > 0 ){

      //Line chart for Revenue
      new Morris.Line({
        element: 'revenue-chart',
        data: user_data,
        xkey: 'created_at',
        ykeys: ['totalrevenue'],
        labels: ['Revenue'],
        dateFormat: function (x) { return new Date(x).toDateString(); },
        preUnits: '$'
      });

      //Line chart for Visits
      new Morris.Line({
        element: 'visits-chart',
        data: user_data,
        xkey: 'created_at',
        ykeys: ['total','premium','npvisits'],
        labels: ['Total','Premium','Non Premium'],
        dateFormat: function (x) { return new Date(x).toDateString(); }
      });

      var total = $('#total-visits h2').text();
      var premium = $('#premium-visits h2').text();
      //Render donut chart for Premium/NonPremium Visits
      new Morris.Donut({
        element: 'distribution-chart',
        data: [
          {value: total-premium, label: 'Non Premium Visits'},
          {value: premium, label: 'Premium Visits'}
        ],
        formatter: function (x) { 
          result =  (x/total)*100
          result = result.toFixed(2)  
          return result+'%'
        }
      })


    }


   }


}