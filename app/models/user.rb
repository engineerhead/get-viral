class User < ActiveRecord::Base	
	has_many :visits
	has_many :referrals
	has_many :payments
	has_one :profile

	accepts_nested_attributes_for :profile

	scope :not_admins, -> { where(admin: false).order('name ASC') }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

end