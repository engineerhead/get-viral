class Payment < ActiveRecord::Base
	belongs_to :user
	has_one :profile, through: :user
	scope :in_range, ->( start, finish, page_param) { where( created_at: start..finish).page(page_param).order( "created_at DESC" ) }
end
