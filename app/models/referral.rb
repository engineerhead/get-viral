class Referral < ActiveRecord::Base
	belongs_to :user
	scope :in_range, ->(start,finish) { where( created_at: start..finish).order( "created_at DESC" ) }
end
