class PaymentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :only_allow_admin, only: [:show, :new, :edit, :create, :update, :destroy]
  before_filter :set_payment, only: [:show, :edit, :update, :destroy]
  before_filter(only: :index)  { set_date_user_params('/payments') }
  
  def index
    if current_user.admin?
      if @user
        @payments = @user.payments.in_range( @start_date, @end_date, params[:page] )
      else
        @payments = Payment.includes(:profile).in_range( @start_date, @end_date, params[:page] )
      end
    else
      @payments = current_user.payments.in_range( @start_date, @end_date, params[:page] )
    end
  end

  def show
  end

  def new
    @payment = Payment.new
  end

  def edit
  end

  def create
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to @payment, notice: 'Payment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @payment }
      else
        format.html { render action: 'new' }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:user_id, :amount, :period, :paid, :paid_on, :invoice)
    end
end
