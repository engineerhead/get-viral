class VisitsController < ApplicationController
  before_filter :authenticate_user!
  before_filter { set_date_user_params('/visits') }

  def index
    if current_user.admin?
      if @user
        @visits = @user.visits.includes(:profile).in_range( @start_date, @end_date )
        @referral_visits = @user.referrals.in_range( @start_date, @end_date )
      else
        @visits = Visit.includes(:profile).in_range( @start_date, @end_date )
        @referral_visits = Referral.in_range( @start_date, @end_date )
      end
    else
      @user = current_user
      @visits = current_user.visits.in_range( @start_date, @end_date )
      @referral_visits = current_user.visits.in_range( @start_date, @end_date )
    end

  end

  
end
