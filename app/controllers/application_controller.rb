class ApplicationController < ActionController::Base
  before_filter :configure_permitted_parameters, if: :devise_controller?
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) << :name
    end
    
    def only_allow_admin
      redirect_to visits_path, alert: 'Not authorized as an administrator.' unless current_user.try(:admin?)
    end

    def after_sign_in_path_for(resource)
     visits_path
    end

    def set_date_user_params(path)
      filtered_params = params[path]
      if filtered_params.blank?
        ref_date = Date.today
        @start_date = ref_date.beginning_of_week
        @end_date = ref_date.end_of_week
      else
        @start_date = Date.parse(filtered_params[:start_date])
        @end_date = Date.parse(filtered_params[:end_date])
        @user = User.find( filtered_params[:user] ) unless filtered_params[:user].blank?
      end
    end
end
