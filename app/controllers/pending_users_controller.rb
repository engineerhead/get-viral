 class PendingUsersController < ApplicationController
  include AnalyticsHelpers

  before_filter :authenticate_user!
  before_filter :only_allow_admin

  def index
  	@pending_visits = PendingTotal.all.page(params[:page])
  end

  def create
    @pending_totals = PendingTotal.all
  	@pending_premium = PendingPremium.all
    
  	@succ = 0

  	pending_user_helper

    redirect_to pending_users_path, notice: "Out of #{@pending_totals.length} pending accounts, #{@succ} were created."
  	  
	end
end