module VisitsHelper
	def get_all_users
		User.not_admins
	end

	def get_total_revenue
		sum = @visits.inject(0) { |sum,n| sum + n.revenue}
		sum.round(2)
	end

	def get_total_visits
		sum = @visits.inject(0) { |sum,n| sum + n.total.to_i}
	end

	def get_premium_visits
		sum = @visits.inject(0) { |sum,n| sum + n.premium.to_i}
	end

	def get_referral_visits
		sum = @referral_visits.inject(0) { |sum,n| sum + n.visits.to_i}
	end

	def get_referral_revenue
		sum = @referral_visits.inject(0) { |sum,n| sum + n.totalrevenue}
		sum.round(2)
	end
end
