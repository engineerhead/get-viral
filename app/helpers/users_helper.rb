module UsersHelper
	
	def get_platinium_users
		User.includes(:profile).where('profiles.level = ?', 'Platinium').references(:profiles)
	end

	def user_levels
		["Platinium", "Gold", "Silver"]
	end

end
