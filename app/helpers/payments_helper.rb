module PaymentsHelper
	def payment_status
		['Paid','Pending']
	end

	def get_balance
		payment = @payments.find { |e| e.paid == false }
		number_to_currency(payment.amount) unless payment.nil?
	end

	def get_last_payout
		payment = @payments.find { |e| e.paid == true }
		number_to_currency(payment.amount) unless payment.nil?
	end
end
