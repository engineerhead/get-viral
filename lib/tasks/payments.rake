namespace :payments do
	
	@today = Date.today.to_s
	
	desc "Calculates payments for past week"
	task :calculate, [:date] => [:environment] do |t,args|
		args.with_defaults( date: @today)	

		# Parse Date for Week
		date = Date.parse(args.date)

		bw = date.beginning_of_week 
		ew = date.end_of_week
		period = bw.strftime("%d/%m/%Y") + '-' + ew.strftime("%d/%m/%Y")

		normal_revenues = Hash.new(0)
		referral_revenues = Hash.new(0)

		all_visits = Visit.select('user_id,revenue').in_range(bw,ew)
		all_visits.each do |e|  
			normal_revenues[e.user_id] += e.revenue
		end

		referral_visits = Referral.select('user_id,revenue').in_range(bw,ew)
		referral_visits.each do |e|  
			referral_revenues[e.user_id] += e.revenue
		end

		ActiveRecord::Base.transaction do
			normal_revenues.each do |id,revenue|
				Payment.create(
					user_id: id,
					amount: revenue.round(2),
					period: period,
					paid: false
				)
			end
			referral_revenues.each do |id,revenue|
				Payment.create(
					user_id: id,
					amount: revenue.round(2),
					period: period,
					paid: false
				)
			end
		end
	end
end