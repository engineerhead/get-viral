namespace :analytics do

	@date_string = Date.today.prev_day.strftime('%Y-%m-%d')

  desc "Gets visits from Google Analytics' API"
  task :fetch, [:date] => [:environment] do |t,args|

		args.with_defaults( date: @date_string)
		
		service_account_email = '973084105848-ll2i5qqnco0agc4ul6lr7j9q1lag0qf5@developer.gserviceaccount.com' # Email of service account
		key_file = Rails.root.join('config', 'keys', 'privatekey.p12')
		key_secret = 'notasecret' # Password to unlock private key
		profileID = '74060027' # Analytics profile ID.

	  client = Google::APIClient.new( application_name: 'Get Viral', application_version: '0.1')

	  analytics = client.discovered_api('analytics','v3')

		# Load our credentials for the service account
		key = Google::APIClient::KeyUtils.load_from_pkcs12(key_file, key_secret)
		client.authorization = Signet::OAuth2::Client.new(
		  :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
		  :audience => 'https://accounts.google.com/o/oauth2/token',
		  :scope => 'https://www.googleapis.com/auth/analytics.readonly',
		  :issuer => service_account_email,
		  :signing_key => key)

		# Request a token for our service account
		client.authorization.fetch_access_token!

		# Params to get visits for premium countries
		get_premium = {
			:api_method => analytics.data.ga.get, 
			:parameters => { 
		  	'ids' => "ga:" + profileID, 
		  	'start-date' => args.date,
		  	'end-date' => args.date,
		  	'metrics' => "ga:visits",
		  	'dimensions' => "ga:campaign,ga:medium",
		  	'filters' => "ga:campaign!=(not set);ga:source==FB;ga:visits>0;ga:country==United States,ga:country==Canada,ga:country==United Kingdom,ga:country==Australia",
		  	'max-results' => 5000
			}
		}

		# Params to get visits from all coutries
		get_total = {
			:api_method => analytics.data.ga.get, 
			:parameters => { 
		  	'ids' => "ga:" + profileID, 
		  	'start-date' => args.date,
		  	'end-date' => args.date,
		  	'metrics' => "ga:visits",
		  	'dimensions' => "ga:campaign",
		  	'filters' => "ga:campaign!=(not set);ga:source==FB;ga:visits>0",
		  	'max-results' => 5000
			}
		}

		# New Batch Request
		batch = Google::APIClient::BatchRequest.new

		batch.add(get_premium) do |result|
		  @premium_visits = result.data.rows.collect { |e| [e.first, e.last.to_i*95/100] }
		end

		batch.add(get_total) do |result|
		  @total_visits = result.data.rows.collect { |e| [e.first, e.last.to_i*95/100] }
		end

		client.execute(batch)

  end

  desc "Inserts visits in DB"
  task :insert,[:date] => [:environment, :fetch] do |t, args|
    include AnalyticsHelpers
  	# Set default date to yesterday
  	args.with_defaults( date: @date_string)	

  	# Parse date string for created_at
  	date = DateTime.parse(args.date)

    rake_helper( @total_visits, @premium_visits, date)

  end


end
