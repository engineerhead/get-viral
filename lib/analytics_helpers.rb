module AnalyticsHelpers
	def rake_helper( total_visits, premium_visits, date )
		@date = date
		@users = User.includes(:profile).where( admin: false)


    total_visits.each do |tv|
    	@tvisits = tv.last 
      @campaign = tv.first
         
      # Search if User with given @campaign exists
      @user = @users.find {|u| u.profile.campaign == @campaign}

      # Search for premium visits with given @campaign
      @related_premium = premium_visits.find_all { |t| t.first == @campaign }

      if @user.nil?
        # Create pending premium/total visits for unknown user
        pending_insertions
      else
      	@p_visits = @related_premium.inject(0) { |sum,n| sum + n.last.to_f}
      	@p_rate = @user.profile.rate
      	
      	@np_visits = @tvisits.to_f - @p_visits
      	@np_rate = @user.profile.nprate
				
				normal_visits_revenue_handler      	
				referral_visits_revenue_handler
      end
    end
	end

	def pending_user_helper
		
		@users = User.includes(:profile).where(admin: false)
		
		@pending_totals.each do |pt|
			@tvisits = pt.visits
			@campaign = pt.campaign
			@date = pt.created_at

			@user = @users.find {|u| u.profile.campaign == @campaign}

	    unless @user.nil?
	      # Revenue calculation for premium visits
	      related_premium = @pending_premium.find_all { |t| t.campaign == @campaign && t.created_at == pt.created_at }
	      @p_visits = related_premium.inject(0) { |sum,n| sum + (n.visits.to_f)}
	      
	      @p_rate = @user.profile.rate
	      
	      @np_visits = @tvisits.to_f - @p_visits
	      @np_rate = @user.profile.nprate

	      normal_visits_revenue_handler
	      referral_visits_revenue_handler

	      related_premium.each  {|pp| pp.delete}
	      pt.delete
	      @succ+=1

			end
		end
	end

	private
	
	def pending_insertions
		@related_premium.each do |rp|
		  PendingPremium.create(
		    campaign: rp.first,
		    source: rp.at(1),
		    visits: rp.last,
		    created_at: @date
		  )
		end
		
		PendingTotal.create(
		  campaign: @campaign,
		  visits: @tvisits,
		  created_at: @date
		)
	end

	def normal_visits_revenue_handler
		p_revenue = revenue_calculation( @p_visits, @p_rate)
		np_revenue = revenue_calculation( @np_visits, @np_rate )
		total_revenue = p_revenue + np_revenue

		@user.visits.create( 
		  total: @tvisits, 
		  premium: @p_visits, 
		  revenue: p_revenue.round(2), 
		  npvisits: @np_visits,
		  nprevenue: np_revenue.round(2),
		  totalrevenue: total_revenue.round(2),
		  created_at: @date
		 )
	end

	def referral_visits_revenue_handler
    platinium_users = @users.find_all {|u| u.profile.level == 'Platinium'}

		referral_id = @user.profile.referral_id || 0
		pl_user = platinium_users.find {|u| u.id == referral_id }
		
		unless pl_user.nil?
		  p_referral_rate = pl_user.profile.rate - @p_rate
		  p_referral_revenue = revenue_calculation( @p_visits, p_referral_rate)
		  np_referral_rate = pl_user.profile.nprate - @np_rate
		  np_referral_revenue = revenue_calculation( @np_visits, np_referral_rate)
		  
		  pl_user.referrals.create(
		    campaign: @campaign,
		    source: pl_user.profile.source,
		    visits: @p_visits,
		    revenue: p_referral_revenue.round(2),
		    npvisits: @np_visits,
		    nprevenue: np_referral_revenue.round(2),
		    totalrevenue: p_referral_revenue + np_referral_revenue.round(2),
		    created_at: @date
		  )            
		end
	end

	def revenue_calculation(visits,rate)
		(visits/1000) * rate
	end

end